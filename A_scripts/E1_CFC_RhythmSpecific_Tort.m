% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/A_dataIn/'];

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/T_tools/PACmeg-master'))

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% accumulate PAC across occipital channels

idxChanGamma = [58:60];

GammaByAlpha = [];
TotalByAlpha = [];
FreqSignalByAlpha = [];
GammaByAlpha_Arh = [];
TotalByAlpha_Arh = [];
FreqSignalByAlpha_Arh = [];
PAC = [];
for indID = 1:numel(IDs)
    disp(num2str(indID))
    PeriEpisodeERPMerged_trial_onset = [];
    %PeriEpisodeERPMerged_trial_offset = [];
    PeriEpisodeERPMerged_trial_onset_notch = [];
    %PeriEpisodeERPMerged_trial_offset_notch = [];
    for indCond = 1:4
       load([pn.dataOut, IDs{indID}, '_PeriEpisode_Freq_8_15_L',num2str(indCond),'.mat'], 'PeriEpisodeERP')
       PeriEpisodeERPMerged_trial_onset = cat(1, PeriEpisodeERPMerged_trial_onset, cat(1,PeriEpisodeERP.trial_onset{idxChanGamma,1}));
       %PeriEpisodeERPMerged_trial_offset = cat(1, PeriEpisodeERPMerged_trial_offset, cat(1,PeriEpisodeERP.trial_offset{idxChanGamma,1}));
       PeriEpisodeERPMerged_trial_onset_notch = cat(1, PeriEpisodeERPMerged_trial_onset_notch, cat(1,PeriEpisodeERP.trial_onset_notch{idxChanGamma,1}));
       %PeriEpisodeERPMerged_trial_offset_notch = cat(1, PeriEpisodeERPMerged_trial_offset_notch, cat(1,PeriEpisodeERP.trial_offset_notch{idxChanGamma,1}));
    end
    
    %% align to negative peak (defined based on phase local minimum; cf. Canolty)
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 
    % use only the phase minima durign 250 ms following alpha onset (can try the opposite later)
    sigphase = sigphase(:,251:end-125);
    %sigphase = sigphase(:,126:251);
    
    % get all local minima < -pi+.01
    
    phaseCrit = sigphase<-pi+.01;
    localminCrit = islocalmin(sigphase,2);
    totalCrit = phaseCrit+localminCrit ==2;
    %figure; imagesc(totalCrit(1:30,:))
    
    [xInd, yInd] = find(totalCrit==1);
    
    %NotchSignalLocked = NaN(numel(xInd),250);
    OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        %NotchSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset_notch(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
        

    %% get gamma power estimates
    % linear FIR, implemented via filtfilt
    % order: 3* ratio(Fs/lowpass)
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S15_microstates/T_tools/eeglab14_1_1b/functions/sigprocfunc/'); % add eegfilt.m
    
    centerFreq = 40:2:150;
    bandFreq = [centerFreq-.2*centerFreq;centerFreq+.2*centerFreq];
    fs = 500;
    filtOrder = 3.*[floor(fs./bandFreq(2,:))]; % internally computed
    FilterResults = [];
    for indFreq = 1:numel(centerFreq)
        % 1. Bandpass for different center frequencies
        tmp = eegfilt(PeriEpisodeERPMerged_trial_onset, fs, bandFreq(1,indFreq), bandFreq(2,indFreq),...
            0,0,0,'fir1');
        %tmp = bandpass(PeriEpisodeERPMerged_trial_onset', bandFreq(:,indFreq), fs);
        % 2. Normalize; Hilbert transform, magnitude, power
        tmp = zscore(tmp',[],1);
        tmp = abs(hilbert(tmp)).^2;
        FilterResults(indFreq,:,:) = tmp'; clear tmp;
    end
    
    FreqSignalLocked = NaN(size(FilterResults,1),numel(xInd),250);
    for indSignal = 1:numel(xInd)
        FreqSignalLocked(:,indSignal,:) = FilterResults(:, xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
    
    GammaByAlpha(indID, :) = squeeze(nanmean(nanmean(FreqSignalLocked(1:end,:,:),2),1));
    TotalByAlpha(indID, :) = squeeze(nanmean(OrigSignalLocked,1));
    FreqSignalByAlpha(indID, :,:) = squeeze(nanmean(FreqSignalLocked,2));

    % calculate MI
    % Apply MVL algorith, from Canolty et al., (2006)
    nbin = 18;
    MI_tmp = []; Phase = sigphase;
    for indFreq = 1:numel(centerFreq)
        for indTrial = 1:size(Phase,1)
            Amp = squeeze(FilterResults(indFreq,indTrial,251:end-125))';
            [MI_tmp(indFreq, indTrial), MeanAmp(indFreq, indTrial,:)] = calc_MI_tort(Phase(indTrial,:),Amp,nbin);
            MVL_tmp(indFreq, indTrial) = abs(mean(Amp.*exp(1i*Phase(indTrial,:))));
        end
    end
    PAC.MI(indID,:) = squeeze(nanmean(MI_tmp,2)); clear MI_tmp;
    PAC.MVL(indID,:) = squeeze(nanmean(MVL_tmp,2)); clear MVL_tmp;
    PAC.MeanAmp(indID,:,:) = squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
    % perform permutations
    for indPerm = 1:1000
        for indFreq = 1:numel(centerFreq)
            permAmpTrial = randperm(size(Phase,1));
            %permPhaseTrial = randperm(size(Phase,1));
            Amp = squeeze(FilterResults(indFreq,permAmpTrial(1),251:end-125))';
            curPhase = Phase(permAmpTrial(end),:);
            [MI_perm_tmp(indFreq, indPerm), MeanAmp(indFreq, indPerm,:)] = calc_MI_tort(curPhase,Amp,nbin);
            MVL_perm_tmp(indFreq, indPerm) = abs(mean(Amp.*exp(1i*curPhase)));
        end
    end
    PAC.MI_perm(indID,:) = PAC.MI(indID,:)-squeeze(nanmean(MI_perm_tmp,2))'; clear MI_perm_tmp;
    PAC.MVL_perm(indID,:) = PAC.MVL(indID,:)-squeeze(nanmean(MVL_perm_tmp,2))'; clear MVL_perm_tmp;
    PAC.MeanAmpPerm(indID,:,:) = squeeze(PAC.MeanAmp(indID,:,:))-squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
%     time = -125/500:1/500:125/500; time = time(2:end).*1000;
%     figure; hold on; 
%     plot(time,squeeze(nanmean(OrigSignalLocked,1)),'k', 'LineWidth', 2); ylabel('Mean signal amplitude (normalized)');
%     yyaxis right
%     plot(time,squeeze(nanmean(nanmean(FreqSignalLocked(10:25,:,:),2),1)), 'LineWidth', 2); 
%     ylabel('Gamma amplitude (70-140 Hz; normalized)');
%     set(findall(gcf,'-property','FontSize'),'FontSize',20)
%     xlabel('ms from alpha trough')
% 
%     
%     figure; 
%     imagesc(squeeze(nanmean(FilterResults,1)),[0 2])
   
    %% for pre-alpha
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 
    sigphase = sigphase(:,126:251);
    
    % get all local minima < -pi+.01
    
    phaseCrit = sigphase<-pi+.01;
    localminCrit = islocalmin(sigphase,2);
    totalCrit = phaseCrit+localminCrit ==2;
    %figure; imagesc(totalCrit(1:30,:))
    
    [xInd, yInd] = find(totalCrit==1);
    
    %NotchSignalLocked = NaN(numel(xInd),250);
    OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        %NotchSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset_notch(xInd(indSignal), 125+yInd(indSignal)-125:125+yInd(indSignal)+124);
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 125+yInd(indSignal)-125:125+yInd(indSignal)+124);
    end
    
    FreqSignalLocked = NaN(size(FilterResults,1),numel(xInd),250);
    for indSignal = 1:numel(xInd)
        FreqSignalLocked(:,indSignal,:) = FilterResults(:, xInd(indSignal), 126+yInd(indSignal)-125:126+yInd(indSignal)+124);
    end
    
    GammaByAlpha_Arh(indID, :) = squeeze(nanmean(nanmean(FreqSignalLocked(1:end,:,:),2),1));
    TotalByAlpha_Arh(indID, :) = squeeze(nanmean(OrigSignalLocked,1));
    FreqSignalByAlpha_Arh(indID, :,:) = squeeze(nanmean(FreqSignalLocked,2));
    
    % calculate MI
    % Apply the algorithm from Ozkurt et al., (2011)
    MI_tmp = []; Phase = sigphase;
    for indFreq = 1:numel(centerFreq)
        for indTrial = 1:size(Phase,1)
            Amp = squeeze(FilterResults(indFreq,indTrial,126:251))';
            [MI_tmp(indFreq, indTrial), MeanAmp(indFreq, indTrial,:)] = calc_MI_tort(Phase(indTrial,:),Amp,nbin);
            MVL_tmp(indFreq, indTrial) = abs(mean(Amp.*exp(1i*Phase(indTrial,:))));
        end
    end
    PAC.MI_preAlpha(indID,:) = squeeze(nanmean(MI_tmp,2)); clear MI_tmp;
    PAC.MVL_preAlpha(indID,:) = squeeze(nanmean(MVL_tmp,2)); clear MVL_tmp;
    PAC.MeanAmp_preAlpha(indID,:,:) = squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
    % perform permutations
    for indPerm = 1:1000
        for indFreq = 1:numel(centerFreq)
            permAmpTrial = randperm(size(Phase,1));
            %permPhaseTrial = randperm(size(Phase,1));
            curPhase = Phase(permAmpTrial(end),:);
            Amp = squeeze(FilterResults(indFreq,permAmpTrial(1),126:251))';
            [MI_perm_tmp(indFreq, indPerm), MeanAmp(indFreq, indPerm,:)] = calc_MI_tort(curPhase,Amp,nbin);
            MVL_perm_tmp(indFreq, indPerm) = abs(mean(Amp.*exp(1i*curPhase)));
        end
    end
    PAC.MI_perm_preAlpha(indID,:) = PAC.MI_preAlpha(indID,:)-squeeze(nanmean(MI_perm_tmp,2))'; clear MI_perm_tmp;
    PAC.MVL_perm_preAlpha(indID,:) = PAC.MVL_preAlpha(indID,:)-squeeze(nanmean(MVL_perm_tmp,2))'; clear MVL_perm_tmp;
    PAC.MeanAmpPerm_preAlpha(indID,:,:) = squeeze(PAC.MeanAmp_preAlpha(indID,:,:))-squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
end

time = -125/500:1/500:125/500; time = time(2:end).*1000;
freqs = 40:2:150;

GammaByAlpha2 = squeeze(nanmean(FreqSignalByAlpha(1:end,freqs>=60 & freqs<=150,:),2));
GammaByAlpha_Arh2 = squeeze(nanmean(FreqSignalByAlpha_Arh(1:end,freqs>=60 & freqs<=150,:),2));
%GammaByAlpha2(20,:) = NaN;

% sort by maximal power within 100:120

[~, sortInd] = sort(nanmean(GammaByAlpha2(:,100:120),2), 'descend');

figure; 
subplot(2,2,1); imagesc(TotalByAlpha(sortInd,:))
subplot(2,2,2); imagesc(GammaByAlpha(sortInd,:))
subplot(2,2,3); imagesc(TotalByAlpha_Arh(sortInd,:))
subplot(2,2,4); imagesc(GammaByAlpha_Arh(sortInd,:))


    figure; 
    subplot(3,1,[1,2]); imagesc(time,[],squeeze(nanmean(FreqSignalByAlpha,1))); %xlim([50, 200])
    subplot(3,1,[3]); 
    standError = nanstd(TotalByAlpha,1)./sqrt(size(TotalByAlpha,1));
    shadedErrorBar(time,nanmean(TotalByAlpha,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    ylabel('Mean signal amplitude (normalized)');
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap');
    cBrew = brewermap(1000,'RdBu');
    cBrew = double(flipud(cBrew));
    colormap(cBrew)
    
    figure; 
    subplot(3,1,[1,2]); imagesc(time,[],squeeze(nanmean(FreqSignalByAlpha_Arh,1))); %xlim([50, 200])
    subplot(3,1,[3]); 
    standError = nanstd(TotalByAlpha,1)./sqrt(size(TotalByAlpha,1));
    shadedErrorBar(time,nanmean(TotalByAlpha,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    ylabel('Mean signal amplitude (normalized)');
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap');
    cBrew = brewermap(1000,'RdBu');
    cBrew = double(flipud(cBrew));
    colormap(cBrew)
    
figure;
imagesc(squeeze(nanmean(PAC.MeanAmpPerm,1)))
imagesc(squeeze(nanmean(PAC.MeanAmpPerm_preAlpha,1)))

addpath('/Volumes/Kosciessa/Tools/ploterr');

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
hold on;
dat = squeeze(nanmean(PAC.MeanAmp(:,freqs>=60 & freqs<=150,:),2));
bar(0:20:360-20, nanmean(dat), 'FaceColor',  [.5 .5 .5], 'EdgeColor', 'none', 'BarWidth', 0.8);
% show standard deviation on top
h1 = ploterr(0:20:360-20, nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
set(h1(1), 'marker', 'none'); % remove marker
set(h1(2), 'LineWidth', 4);        
set(gca,'xtick',0:180:360); set(gca,'xTickLabel',{'-pi'; '0'; '+pi'}); xlabel('Alpha Phase (rad.)'); ylabel('Gamma Amplitude')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
ylim([.048 .065])

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
figureName = 'E_gammaCoupling_v2';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
hold on;
dat = squeeze(nanmean(PAC.MeanAmp(:,freqs>=60 & freqs<=120,:)-PAC.MeanAmp_preAlpha(:,freqs>=60 & freqs<=120,:),2));
bar(0:20:360-20, nanmean(dat), 'FaceColor',  [.5 .5 .5], 'EdgeColor', 'none', 'BarWidth', 0.8);
% show standard deviation on top
h1 = ploterr(0:20:360-20, nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
set(h1(1), 'marker', 'none'); % remove marker
set(h1(2), 'LineWidth', 4);        
set(gca,'xtick',0:180:360); set(gca,'xTickLabel',{'-pi'; '0'; '+pi'}); xlabel('Alpha Phase (rad.)'); ylabel('Gamma Amplitude')
set(findall(gcf,'-property','FontSize'),'FontSize',20)
%ylim([1.7 2.2])

% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
% figureName = 'E_alphaGammaBars_abs';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

% figure;
% bar(squeeze(nanmean(nanmean(PAC.MeanAmp(:,10:end,:),2),1)))
% bar(squeeze(nanmean(nanmean(PAC.MeanAmp_preAlpha(:,10:end,:),2),1)))


% add error bars
pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(0, 'DefaultFigureRenderer', 'painters');
subplot(2,1,1); hold on; 
    standError = nanstd(TotalByAlpha,1)./sqrt(size(TotalByAlpha,1));
    shadedErrorBar(time,nanmean(TotalByAlpha,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    ylabel('Mean signal amplitude (normalized)');
    yyaxis right; 
    standError = nanstd(GammaByAlpha2,1)./sqrt(size(GammaByAlpha2,1));
    shadedErrorBar(time,nanmean(GammaByAlpha2,1),standError, 'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .1);
    ylabel('Gamma amplitude (80-140 Hz; normalized)');
    %xlim([-150 150])
subplot(2,1,2); hold on; 
    standError = nanstd(TotalByAlpha_Arh,1)./sqrt(size(TotalByAlpha_Arh,1));
    shadedErrorBar(time,nanmean(TotalByAlpha_Arh,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    %ylabel('Mean signal amplitude (normalized)');
    yyaxis right; 
    standError = nanstd(GammaByAlpha_Arh2,1)./sqrt(size(GammaByAlpha_Arh2,1));
    shadedErrorBar(time,nanmean(GammaByAlpha_Arh2,1),standError, 'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .1);
    %ylabel('Gamma amplitude (70-140 Hz; normalized)');
    %xlim([-150 150])
    xlabel('ms from alpha trough')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
    figureName = 'E_alphaGammaTraces_v2';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
    standError = nanstd(PAC.MI_perm,1)./sqrt(size(PAC.MI_perm,1));
    shadedErrorBar([],nanmean(PAC.MI_perm,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    line([0 25], [0, 0])
% h = figure('units','normalized','position',[.1 .1 .2 .3]);
% set(0, 'DefaultFigureRenderer', 'painters');
    standError = nanstd(PAC.MI_perm_preAlpha,1)./sqrt(size(PAC.MI_perm_preAlpha,1));
    shadedErrorBar([],nanmean(PAC.MI_perm_preAlpha,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    line([0 25], [0, 0])
    
h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
    standError = nanstd(PAC.MVL_perm,1)./sqrt(size(PAC.MVL_perm,1));
    shadedErrorBar([],nanmean(PAC.MVL_perm,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    line([0 25], [0, 0])
% h = figure('units','normalized','position',[.1 .1 .2 .3]);
% set(0, 'DefaultFigureRenderer', 'painters');
    standError = nanstd(PAC.MVL_perm_preAlpha,1)./sqrt(size(PAC.MVL_perm_preAlpha,1));
    shadedErrorBar([],nanmean(PAC.MVL_perm_preAlpha,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    line([0 25], [0, 0])
    
    %% save outputs
    
    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/F1_TortCFC_filtfilt_v2.mat'], ...
        'PAC', 'FreqSignalByAlpha', 'FreqSignalByAlpha_Arh', 'TotalByAlpha', 'GammaByAlpha', 'TotalByAlpha_Arh', 'GammaByAlpha_Arh')
    
    %% plot results with statistics
    
    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/F1_TortCFC_filtfilt_v2.mat'], ...
        'PAC', 'FreqSignalByAlpha', 'FreqSignalByAlpha_Arh', 'TotalByAlpha', 'GammaByAlpha', 'TotalByAlpha_Arh', 'GammaByAlpha_Arh')
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/F2_TortCFC_stats_v2.mat', ...
    'stat_noalpha', 'stat_alpha', 'stat_alpha_mvl', 'stat_noalpha_mvl')

    h = figure('units','normalized','position',[.1 .1 .2 .3]);
    set(0, 'DefaultFigureRenderer', 'painters'); hold on;
    standError = nanstd(PAC.MI_perm,1)./sqrt(size(PAC.MI_perm,1));
    shadedErrorBar(40:2:150,nanmean(PAC.MI_perm,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    %line([0 size(PAC.MI_perm,2)], [0, 0],'LineWidth', 2)
    alphaMask = double(stat_alpha.mask); alphaMask(alphaMask==0) = NaN; alphaMask(alphaMask==1) = 0;
    plot(40:2:150,alphaMask,'Color', 'r', 'LineWidth', 5)
    ylim([-.5 1.8].*10^-3); xlim([40, 150])
    xlabel('Frequency for amplitude'); ylabel('PAC (surrogate-normalized)')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
    figureName = 'E_alpha_stats_v2';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    
    h = figure('units','normalized','position',[.1 .1 .2 .3]);
    set(0, 'DefaultFigureRenderer', 'painters'); hold on;
    standError = nanstd(PAC.MI_perm_preAlpha,1)./sqrt(size(PAC.MI_perm_preAlpha,1));
    shadedErrorBar(40:2:150,nanmean(PAC.MI_perm_preAlpha,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    %line([0 size(PAC.MI_perm,2)], [0, 0],'LineWidth', 2)
    alphaMask = double(stat_noalpha.mask); alphaMask(alphaMask==0) = NaN; alphaMask(alphaMask==1) = 0;
    plot(40:2:150,alphaMask,'Color', 'r', 'LineWidth', 5)
    ylim([-.5 1.8].*10^-3); xlim([40, 150])
    xlabel('Frequency for amplitude'); ylabel('PAC (surrogate-normalized)')
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
    figureName = 'E_noalpha_stats_v2';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

