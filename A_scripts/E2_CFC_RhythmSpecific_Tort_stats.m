% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/A_dataIn/'];

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/T_tools/fieldtrip-20170904'); ft_defaults;

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/F1_TortCFC_filtfilt_v2.mat'], ...
        'PAC', 'FreqSignalByAlpha', 'TotalByAlpha', 'GammaByAlpha', 'TotalByAlpha_Arh', 'GammaByAlpha_Arh')
    
%% CBPA

CBPAdata = [];
for indID = 1:numel(IDs)
    CBPAdata{indID}.data = PAC.MI_perm(indID,:);
    CBPAdata{indID}.dimord = 'freq';
    CBPAdata{indID}.time = 40:4:150;
    CBPAdata{indID}.label{1} = 'PAC';
    NullData{indID} = CBPAdata{indID};
    NullData{indID}.data = zeros(size(NullData{indID}.data)); 
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_alpha] = ft_timelockstatistics(cfgStat, CBPAdata{:}, NullData{:});

figure; imagesc(stat_alpha.mask)

%% find PAC pre-alpha


CBPAdata = [];
for indID = 1:numel(IDs)
    CBPAdata{indID}.data = PAC.MI_perm_preAlpha(indID,:);
    CBPAdata{indID}.dimord = 'freq';
    CBPAdata{indID}.time = 40:4:150;
    CBPAdata{indID}.label{1} = 'PAC';
    NullData{indID} = CBPAdata{indID};
    NullData{indID}.data = zeros(size(NullData{indID}.data)); 
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_noalpha] = ft_timelockstatistics(cfgStat, CBPAdata{:}, NullData{:});

figure; imagesc(stat_noalpha.mask)

%% CBPAs for MVL variants

CBPAdata = [];
for indID = 1:numel(IDs)
    CBPAdata{indID}.data = PAC.MVL_perm(indID,:);
    CBPAdata{indID}.dimord = 'freq';
    CBPAdata{indID}.time = 40:4:150;
    CBPAdata{indID}.label{1} = 'PAC';
    NullData{indID} = CBPAdata{indID};
    NullData{indID}.data = zeros(size(NullData{indID}.data)); 
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_alpha_mvl] = ft_timelockstatistics(cfgStat, CBPAdata{:}, NullData{:});

figure; imagesc(stat_alpha_mvl.mask)

%% find PAC pre-alpha


CBPAdata = [];
for indID = 1:numel(IDs)
    CBPAdata{indID}.data = PAC.MVL_perm_preAlpha(indID,:);
    CBPAdata{indID}.dimord = 'freq';
    CBPAdata{indID}.time = 40:4:150;
    CBPAdata{indID}.label{1} = 'PAC';
    NullData{indID} = CBPAdata{indID};
    NullData{indID}.data = zeros(size(NullData{indID}.data)); 
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_noalpha_mvl] = ft_timelockstatistics(cfgStat, CBPAdata{:}, NullData{:});

figure; imagesc(stat_noalpha_mvl.mask)

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/F2_TortCFC_stats_v2.mat', ...
    'stat_noalpha', 'stat_alpha', 'stat_alpha_mvl', 'stat_noalpha_mvl')

