% Collect individual results

restoredefaultpath;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/A_dataIn/'];

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/T_tools/PACmeg-master'))

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%idxChanGamma = [53:55, 58:60];
idxChanGamma = [58:60];

GammaByAlpha = [];
TotalByAlpha = [];
FreqSignalByAlpha = [];
GammaByAlpha_Arh = [];
TotalByAlpha_Arh = [];
FreqSignalByAlpha_Arh = [];
PAC = [];
for indCond = 1:4
    disp(num2str(indCond))
    PeriEpisodeERPMerged_trial_onset = [];
    PeriEpisodeERPMerged_trial_onset_notch = [];
    for indID = 1:numel(IDs)
       load([pn.dataOut, IDs{indID}, '_PeriEpisode_Freq_8_15_L',num2str(indCond),'.mat'], 'PeriEpisodeERP')
       PeriEpisodeERPMerged_trial_onset = cat(1, PeriEpisodeERPMerged_trial_onset, cat(1,PeriEpisodeERP.trial_onset{idxChanGamma,1}));
       PeriEpisodeERPMerged_trial_onset_notch = cat(1, PeriEpisodeERPMerged_trial_onset_notch, cat(1,PeriEpisodeERP.trial_onset_notch{idxChanGamma,1}));
    end
%% align to negative peak (defined based on phase local minimum; cf. Canolty)
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 
    % use only the phase minima durign 250 ms following alpha onset (can try the opposite later)
    sigphase = sigphase(:,251:end-125);
    %sigphase = sigphase(:,126:251);
    
    % get all local minima < -pi+.01
    
    phaseCrit = sigphase<-pi+.01;
    localminCrit = islocalmin(sigphase,2);
    totalCrit = phaseCrit+localminCrit ==2;
    %figure; imagesc(totalCrit(1:30,:))
    
    [xInd, yInd] = find(totalCrit==1);
    
    %NotchSignalLocked = NaN(numel(xInd),250);
    OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        %NotchSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset_notch(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
        

    %% get gamma power estimates
    % linear FIR, implemented via filtfilt
    % order: 3* ratio(Fs/lowpass)
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S15_microstates/T_tools/eeglab14_1_1b/functions/sigprocfunc/'); % add eegfilt.m
    
    centerFreq = 40:2:150;
    bandFreq = [centerFreq-.2*centerFreq;centerFreq+.2*centerFreq];
    fs = 500;
    filtOrder = 3.*[floor(fs./bandFreq(2,:))]; % internally computed
    FilterResults = [];
    for indFreq = 1:numel(centerFreq)
        % 1. Bandpass for different center frequencies
        tmp = eegfilt(PeriEpisodeERPMerged_trial_onset, fs, bandFreq(1,indFreq), bandFreq(2,indFreq),...
            0,0,0,'fir1');
        %tmp = bandpass(PeriEpisodeERPMerged_trial_onset', bandFreq(:,indFreq), fs);
        % 2. Normalize; Hilbert transform, magnitude, power
        tmp = zscore(tmp',[],1);
        tmp = abs(hilbert(tmp)).^2;
        FilterResults(indFreq,:,:) = tmp'; clear tmp;
    end
    
    FreqSignalLocked = NaN(size(FilterResults,1),numel(xInd),250);
    for indSignal = 1:numel(xInd)
        FreqSignalLocked(:,indSignal,:) = FilterResults(:, xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
    
    GammaByAlpha(indCond, :) = squeeze(nanmean(nanmean(FreqSignalLocked(1:end,:,:),2),1));
    TotalByAlpha(indCond, :) = squeeze(nanmean(OrigSignalLocked,1));
    FreqSignalByAlpha(indCond, :,:) = squeeze(nanmean(FreqSignalLocked,2));

    % calculate MI
    % Apply MVL algorith, from Canolty et al., (2006)
    nbin = 18;
    MI_tmp = []; Phase = sigphase;
    for indFreq = 1:numel(centerFreq)
        for indTrial = 1:size(Phase,1)
            Amp = squeeze(FilterResults(indFreq,indTrial,251:end-125))';
            [MI_tmp(indFreq, indTrial), MeanAmp(indFreq, indTrial,:)] = calc_MI_tort(Phase(indTrial,:),Amp,nbin);
            MVL_tmp(indFreq, indTrial) = abs(mean(Amp.*exp(1i*Phase(indTrial,:))));
        end
    end
    PAC.MI(indCond,:) = squeeze(nanmean(MI_tmp,2)); clear MI_tmp;
    PAC.MVL(indCond,:) = squeeze(nanmean(MVL_tmp,2)); clear MVL_tmp;
    PAC.MeanAmp(indCond,:,:) = squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
    % perform permutations
    for indPerm = 1:1000
        for indFreq = 1:numel(centerFreq)
            permAmpTrial = randperm(size(Phase,1));
            %permPhaseTrial = randperm(size(Phase,1));
            Amp = squeeze(FilterResults(indFreq,permAmpTrial(1),251:end-125))';
            curPhase = Phase(permAmpTrial(end),:);
            [MI_perm_tmp(indFreq, indPerm), MeanAmp(indFreq, indPerm,:)] = calc_MI_tort(curPhase,Amp,nbin);
            MVL_perm_tmp(indFreq, indPerm) = abs(mean(Amp.*exp(1i*curPhase)));
        end
    end
    PAC.MI_perm(indCond,:) = PAC.MI(indCond,:)-squeeze(nanmean(MI_perm_tmp,2))'; clear MI_perm_tmp;
    PAC.MVL_perm(indCond,:) = PAC.MVL(indCond,:)-squeeze(nanmean(MVL_perm_tmp,2))'; clear MVL_perm_tmp;
    PAC.MeanAmpPerm(indCond,:,:) = squeeze(PAC.MeanAmp(indCond,:,:))-squeeze(nanmean(MeanAmp,2)); clear MeanAmp;

    %% for pre-alpha
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 
    sigphase = sigphase(:,126:251);
    
    % get all local minima < -pi+.01
    
    phaseCrit = sigphase<-pi+.01;
    localminCrit = islocalmin(sigphase,2);
    totalCrit = phaseCrit+localminCrit ==2;
    %figure; imagesc(totalCrit(1:30,:))
    
    [xInd, yInd] = find(totalCrit==1);
    
    %NotchSignalLocked = NaN(numel(xInd),250);
    OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        %NotchSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset_notch(xInd(indSignal), 125+yInd(indSignal)-125:125+yInd(indSignal)+124);
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 125+yInd(indSignal)-125:125+yInd(indSignal)+124);
    end
    
    FreqSignalLocked = NaN(size(FilterResults,1),numel(xInd),250);
    for indSignal = 1:numel(xInd)
        FreqSignalLocked(:,indSignal,:) = FilterResults(:, xInd(indSignal), 126+yInd(indSignal)-125:126+yInd(indSignal)+124);
    end
    
    GammaByAlpha_Arh(indCond, :) = squeeze(nanmean(nanmean(FreqSignalLocked(1:end,:,:),2),1));
    TotalByAlpha_Arh(indCond, :) = squeeze(nanmean(OrigSignalLocked,1));
    FreqSignalByAlpha_Arh(indCond, :,:) = squeeze(nanmean(FreqSignalLocked,2));
    
    % calculate MI
    % Apply the algorithm from Ozkurt et al., (2011)
    MI_tmp = []; Phase = sigphase;
    for indFreq = 1:numel(centerFreq)
        for indTrial = 1:size(Phase,1)
            Amp = squeeze(FilterResults(indFreq,indTrial,126:251))';
            [MI_tmp(indFreq, indTrial), MeanAmp(indFreq, indTrial,:)] = calc_MI_tort(Phase(indTrial,:),Amp,nbin);
            MVL_tmp(indFreq, indTrial) = abs(mean(Amp.*exp(1i*Phase(indTrial,:))));
        end
    end
    PAC.MI_preAlpha(indCond,:) = squeeze(nanmean(MI_tmp,2)); clear MI_tmp;
    PAC.MVL_preAlpha(indCond,:) = squeeze(nanmean(MVL_tmp,2)); clear MVL_tmp;
    PAC.MeanAmp_preAlpha(indCond,:,:) = squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
    % perform permutations
    for indPerm = 1:1000
        for indFreq = 1:numel(centerFreq)
            permAmpTrial = randperm(size(Phase,1));
            %permPhaseTrial = randperm(size(Phase,1));
            curPhase = Phase(permAmpTrial(end),:);
            Amp = squeeze(FilterResults(indFreq,permAmpTrial(1),126:251))';
            [MI_perm_tmp(indFreq, indPerm), MeanAmp(indFreq, indPerm,:)] = calc_MI_tort(curPhase,Amp,nbin);
            MVL_perm_tmp(indFreq, indPerm) = abs(mean(Amp.*exp(1i*curPhase)));
        end
    end
    PAC.MI_perm_preAlpha(indCond,:) = PAC.MI_preAlpha(indCond,:)-squeeze(nanmean(MI_perm_tmp,2))'; clear MI_perm_tmp;
    PAC.MVL_perm_preAlpha(indCond,:) = PAC.MVL_preAlpha(indCond,:)-squeeze(nanmean(MVL_perm_tmp,2))'; clear MVL_perm_tmp;
    PAC.MeanAmpPerm_preAlpha(indCond,:,:) = squeeze(PAC.MeanAmp_preAlpha(indCond,:,:))-squeeze(nanmean(MeanAmp,2)); clear MeanAmp;
    
end % condition loop

time = -125/500:1/500:125/500; time = time(2:end).*1000;
freqs = 40:2:150;

GammaByAlpha2_L1 = nanmean(FreqSignalByAlpha(1,freqs>=60 & freqs<=150,:),2);
GammaByAlpha2_L2 = nanmean(FreqSignalByAlpha(2,freqs>=60 & freqs<=150,:),2);
GammaByAlpha2_L3 = nanmean(FreqSignalByAlpha(3,freqs>=60 & freqs<=150,:),2);
GammaByAlpha2_L4 = nanmean(FreqSignalByAlpha(4,freqs>=60 & freqs<=150,:),2);
GammaByAlpha2_L24 = nanmean(nanmean(FreqSignalByAlpha(2:4,freqs>=60 & freqs<=150,:),2),1);

pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);

h = figure('units','normalized','position',[.1 .1 .15 .1]);
set(0, 'DefaultFigureRenderer', 'painters');
cla; hold on; 
    plot(time,squeeze(nanmean(TotalByAlpha(1:4,:,:),1)), 'k', 'Linewidth', 2);
    ylabel('Mean signal amplitude (normalized)');
	yyaxis right; 
    plot(time,squeeze(nanmean(GammaByAlpha2_L1,1)),'-', 'Color',6.*[.15 .1 .1], 'Linewidth', 2);
    plot(time,squeeze(nanmean(GammaByAlpha2_L2,1)),'-', 'Color',5.*[.2 .1 .1], 'Linewidth', 2);
    plot(time,squeeze(nanmean(GammaByAlpha2_L3,1)),'-', 'Color',3.*[.3 .1 .1], 'Linewidth', 2);
    plot(time,squeeze(nanmean(GammaByAlpha2_L4,1)),'-', 'Color',2.*[.3 .1 .1], 'Linewidth', 2);
    xlim([-60 60])
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
figureName = 'E5_combinedLoad_v2';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

    figure; imagesc(squeeze(nanmean(PAC.MeanAmpPerm,2)))
    figure; imagesc(squeeze(nanmean(PAC.MeanAmpPerm_preAlpha,2)))

