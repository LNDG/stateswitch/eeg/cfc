function A_extractPeriRhythmTimeTFR

% Note: channels refer to a pair of 'rhythm detected' & the TFR at that
% channel. As the onsets may vary between channels, this means that the TFR
% content may also temporally vary across channels.

%% set up paths

% % convert from UNIX input
% indID = str2num(indID);
% indFreq = str2num(indFreq);

indFreq = 2;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.preprocData  = [pn.root, 'X1_preprocEEGData/'];
pn.dataIn      = [pn.root, 'S5D_eBOSC_CSD_Stim_v2/A_scripts/D2_PeriRhythmDynamics_v2/B_data/'];
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/Z/A_dataIn/']; mkdir(pn.dataOut)
addpath([pn.root, 'S5D_eBOSC_CSD_Stim_v2/A_scripts/D2_PeriRhythmDynamics_v2/T_tools/']);
addpath([pn.root, 'S5D_eBOSC_CSD_Stim_v2/A_scripts/D2_PeriRhythmDynamics_v2/T_tools/fieldtrip-20170904']); ft_defaults;

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for indID = 1:numel(IDs)

    %% load preproc & eBOSC data

    load([pn.preprocData, IDs{indID}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']); % load data

    load([pn.dataIn, 'A_eventTiming/', IDs{indID},  '_EpisodeTiming.mat'])
    
    %% preproc
    cfg.eBOSC.waveseg  = [0 9]; % include +-3s around stim processing; 3 seconds will be cut at each end during detection --> 3 to 6 (stim only)

    conditions= {'dim1';'dim2';'dim3';'dim4'};

    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;

    %% remove extraneous channels

    rem             = [];
    rem.channel     = {'all','-IOR','-LHEOG','-RHEOG','-A1','-A2'};
    rem.demean      = 'no';

    dataNew = ft_preprocessing(rem, data); clear rem data;
    dataNew.TrlInfo = TrlInfo;
    dataNew.TrlInfoLabels = TrlInfoLabels;

    %% define conditions and loop across them

    for c = 1:length(conditions)

        %% segment data according to condition

        tmp.idx = find([TrlInfo(:,8)] == str2num(conditions{c}(end)));
        tmp.NOidx = find([TrlInfo(:,8)] ~= str2num(conditions{c}(end)));

        % rebuild data.Reref according to condition data

        data.(conditions{c}) = dataNew;
        data.(conditions{c}).sampleinfo(tmp.NOidx,:) = [];
        data.(conditions{c}).trial(:,tmp.NOidx) = [];
        data.(conditions{c}).time(:,tmp.NOidx) = [];
        data.(conditions{c}).TrlInfo = TrlInfo(tmp.idx,:);
        data.(conditions{c}).TrlInfoLabels = TrlInfoLabels;

        TrlInfoCond = data.(conditions{c}).TrlInfo;
        TrlInfoLabelsCond = data.(conditions{c}).TrlInfoLabels;

        %% Redefine data to retention interval

        dataFull.(conditions{c}) = data.(conditions{c}); % save original for calculation of BG

        % data for BOSC (includes extensive peri-retention data)

        cfg.begsample = find(data.(conditions{c}).time{1,1} > cfg.eBOSC.waveseg(1), 1, 'first');
        cfg.endsample = find(data.(conditions{c}).time{1,1} <= cfg.eBOSC.waveseg(2), 1, 'last');

        data.(conditions{c}) = ft_redefinetrial(cfg, data.(conditions{c}));

        %% apply CSD transformation

        % CSD transform
        csd_cfg = [];
        csd_cfg.elecfile = 'standard_1005.elc';
        csd_cfg.method = 'spline';
        data.(conditions{c}) = ft_scalpcurrentdensity(csd_cfg, data.(conditions{c}));
        data.(conditions{c}).TrlInfo = TrlInfoCond; clear TrlInfoCond;
        data.(conditions{c}).TrlInfoLabels = TrlInfoLabelsCond; clear TrlInfoLabelsCond;
        
        %% 5-15 Hz BPF filter
        
        % bpf-filter the 5-15 Hz alpha band
        cfg_hpf.bpfilter = 'yes';
        cfg_hpf.bpfreq = [5 15];
        cfg_hpf.bpfiltord = 6;
        [data_HPF.(conditions{c})] = ft_preprocessing(cfg_hpf, data.(conditions{c}));

    end; clear c; % condition loop

%%  parameters for TFR (same as used for eBOSC)

    TFRparam.F           = 2.^[0:.125:6];
    TFRparam.wavenumber  = 5;
    TFRparam.fsample     = 500;
    TFRparam.trialPad    = 1500;

%% create TFR & encode rhythm-dependent results

    cond = conditions;

    freqRanges = [3, 8; 8, 15; 15, 25; 25, 64];
    
    for indCond = 1:4
        
        % initiate output structure
        PeriEpisodeERP = data.dim1;
        PeriEpisodeERP.trial = [];
        PeriEpisodeERP = rmfield(PeriEpisodeERP, 'TrlInfo');
        PeriEpisodeERP = rmfield(PeriEpisodeERP, 'TrlInfoLabels');
        
        for indRhythmChan = 1:60 % this is the index for the channel at which rhythmicity is detected        
           
            time = data.dim1.time{1};
            tmp_onset = find(time >= 3,1, 'first');
            tmp_offset = find(time <= 6,1, 'last');

            PeriEpisodeERP.time = time(tmp_onset:tmp_offset);

            tmp_data = cat(3, data.(cond{indCond}).trial{:});    
            PeriEpisodeERP.trial_onset{indRhythmChan,1} = squeeze(permute(tmp_data(indRhythmChan,tmp_onset:tmp_offset,:),[3,2,1]));
            
            tmp_data = cat(3, data_HPF.(cond{indCond}).trial{:});
            PeriEpisodeERP.trial_onset_bpf{indRhythmChan,1} = squeeze(permute(tmp_data(indRhythmChan,tmp_onset:tmp_offset,:),[3,2,1]));
                    
        end

        % save condition-specific output
        save([pn.dataOut, IDs{indID}, '_PeriEpisode_Freq_', ...
            num2str(freqRanges(indFreq,1)), '_',num2str(freqRanges(indFreq,2)), '_L',num2str(indCond),'.mat'], ...
            'PeriEpisodeERP')
    end

end