% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/Z/A_dataIn/'];

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/T_tools/PACmeg-master'))

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%idxChanGamma = [53:55, 58:60];
idxChanGamma = [58:60];

GammaLocked = [];
for indID = 1:numel(IDs)
    disp(num2str(indID))
    PeriEpisodeERPMerged_trial_onset = [];
    PeriEpisodeERPMerged_trial_onset_pbf = [];
    for indCond = 1:4
       load([pn.dataOut, IDs{indID}, '_PeriEpisode_Freq_8_15_L',num2str(indCond),'.mat'], 'PeriEpisodeERP')
       PeriEpisodeERPMerged_trial_onset = cat(1, PeriEpisodeERPMerged_trial_onset, cat(1,PeriEpisodeERP.trial_onset{idxChanGamma,1}));
       PeriEpisodeERPMerged_trial_onset_pbf = cat(1, PeriEpisodeERPMerged_trial_onset_pbf, cat(1,PeriEpisodeERP.trial_onset_bpf{idxChanGamma,1}));
    end
    
    %% align to negative peak (defined based on phase local minimum; cf. Canolty)
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset_pbf); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 

    %% get gamma power estimates
    
    bandFreq = [60, 140];
   
    % 1. Bandpass for different center frequencies
    gammaFilt = bandpass(PeriEpisodeERPMerged_trial_onset', bandFreq, 500)';
    % select local maxima within 50 ms windows
    % minimum distance: 25 samples
    % use only the phase minima durign 250 ms following alpha onset (can try the opposite later)
    localminCrit = islocalmax(gammaFilt(:,250:end-250),2, 'MinSeparation', 50);
    [xInd, yInd] = find(localminCrit==1);
    
    OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
    
    GammaLocked(indID,:) = squeeze(nanmean(OrigSignalLocked,1));
%   figure; plot(squeeze(nanmean(OrigSignalLocked,1)));
   
end

figure; plot(squeeze(nanmean(GammaLocked([13,21,24,42],:),1)));
figure; plot(squeeze(nanmean(GammaLocked([13,21,24,42],:),1)));
figure; plot(squeeze(nanmean(GammaLocked([24],:),1)));
figure; plot(squeeze(nanmean(GammaLocked([5],:),1)));
figure; plot(squeeze(nanmean(GammaLocked(:,:),1)));

figure; plot(squeeze(nanmean(zscore(GammaLocked(:,:),[],2),1)));

figure; plot(squeeze(GammaLocked)');
figure;imagesc(squeeze(GammaLocked));

%% load complete data to calculate inter-individual max. modulators

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/Z/F1_TortCFC_filtfilt_v2.mat'], ...
        'PAC', 'FreqSignalByAlpha', 'TotalByAlpha', 'GammaByAlpha')
    
time = -125/500:1/500:125/500; time = time(2:end).*1000;
freqs = 40:2:150;
GammaByAlpha2 = squeeze(nanmean(FreqSignalByAlpha(1:end,freqs>=80 & freqs<=120,:),2));

% sort by maximal power within 100:120

[~, sortInd] = sort(nanmean(GammaByAlpha2(:,100:120),2), 'descend');
figure; 
subplot(1,2,1); imagesc(TotalByAlpha(sortInd,:))
subplot(1,2,2); imagesc(GammaByAlpha(sortInd,:))

figure; plot(squeeze(nanmean(GammaLocked(sortInd(1:10),:),1)));

figure; imagesc(zscore(GammaLocked(sortInd(1:10),:),[],2), [-2 2])