% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/A_dataIn/'];

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/T_tools/fieldtrip-20170904'); ft_defaults;
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/Z/F1_TortCFC_filtfilt_allChans.mat'], ...
        'PAC', 'FreqSignalByAlpha', 'TotalByAlpha', 'GammaByAlpha')
    
%% CBPA

CBPAdata = [];
for indID = 1:numel(IDs)
    CBPAdata{indID}.data = squeeze(PAC.MI_perm(indID,:,:));
    CBPAdata{indID}.dimord = 'chan_freq';
    CBPAdata{indID}.time = 40:4:150;
    CBPAdata{indID}.label = cellstr(elec.label(:));
    NullData{indID} = CBPAdata{indID};
    NullData{indID}.data = zeros(size(NullData{indID}.data)); 
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_alpha] = ft_timelockstatistics(cfgStat, CBPAdata{:}, NullData{:});

figure; imagesc(squeeze(stat_alpha.mask))
figure; imagesc(squeeze(stat_alpha.stat))
figure; imagesc(squeeze(stat_alpha.mask.*stat_alpha.stat))

%% plot topography

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap');

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    %figure; imagesc(stat.stat.*stat.mask)

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = stat.label(stat.mask);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 40;
    cfg.zlim = [-5 5];
    cfg.style = 'both';
    cfg.colormap = cBrew;

    h = figure('units','normalized','position',[.1 .1 .2 .2]); 
    plotData = [];
    plotData.label = stat_alpha.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_alpha.stat,2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t values');
    pval = []; pval = convertPtoExponential(stat.posclusters(2).prob);
    title({'Linear effect 1/f slopes', ['p = ', pval{1}]});
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    

%% find PAC pre-alpha

CBPAdata = [];
for indID = 1:numel(IDs)
    CBPAdata{indID}.data = PAC.MVL_perm(indID,:);
    CBPAdata{indID}.dimord = 'freq';
    CBPAdata{indID}.time = 40:4:150;
    CBPAdata{indID}.label{1} = 'PAC';
    NullData{indID} = CBPAdata{indID};
    NullData{indID}.data = zeros(size(NullData{indID}.data)); 
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_mvl] = ft_timelockstatistics(cfgStat, CBPAdata{:}, NullData{:});

figure; imagesc(stat_mvl.mask)

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/B_data/Z/F2_TortCFC_stats_v2.mat', 'stat_mvl', 'stat_alpha')

