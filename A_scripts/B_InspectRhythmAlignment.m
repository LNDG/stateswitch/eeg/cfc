% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/A_dataIn/'];

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

PeriEpisodeERPMerged_trial_onset = [];
PeriEpisodeERPMerged_trial_offset = [];
PeriEpisodeERPMerged_trial_onset_notch = [];
PeriEpisodeERPMerged_trial_offset_notch = [];
for indID = 1:numel(IDs)
    for indCond = 1:4
        load([pn.dataOut, IDs{indID}, '_PeriEpisode_Freq_8_15_L',num2str(indCond),'.mat'], 'PeriEpisodeERP')
       PeriEpisodeERPMerged_trial_onset = cat(1, PeriEpisodeERPMerged_trial_onset, cat(1,PeriEpisodeERP.trial_onset{58:60,1}));
       PeriEpisodeERPMerged_trial_offset = cat(1, PeriEpisodeERPMerged_trial_offset, cat(1,PeriEpisodeERP.trial_offset{58:60,1}));
       PeriEpisodeERPMerged_trial_onset_notch = cat(1, PeriEpisodeERPMerged_trial_onset_notch, cat(1,PeriEpisodeERP.trial_onset_notch{58:60,1}));
       PeriEpisodeERPMerged_trial_offset_notch = cat(1, PeriEpisodeERPMerged_trial_offset_notch, cat(1,PeriEpisodeERP.trial_offset_notch{58:60,1}));
    end
end
    
%% sort according to instantaneous phase

smoothIndex = 1;
%clim = [-5*10^-4, 5*10^-4];
clim = [-2 2];

sortAtSample_onset = 300;
alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);
y = hilbert(alphaFiltered_onset); sigphase = angle(y); [~, sortInd_onset] = sort(sigphase(:,sortAtSample_onset), 'ascend');

sortAtSample_offset = 200;
alphaFiltered_offset = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); 
alphaFiltered_offset = zscore(alphaFiltered_offset, [], 2);
y = hilbert(alphaFiltered_offset); sigphase = angle(y); [~, sortInd_offset] = sort(sigphase(:,sortAtSample_offset), 'ascend');

figure; 
subplot(3,2,1);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_onset,:), clim)
    title('Transition to alpha onset')
subplot(3,2,2);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_offset,:), clim)
    title('Transition to alpha offset')
subplot(3,2,3);
    x = squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_onset,:), clim)
subplot(3,2,4);
    x = squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_offset,:), clim)
subplot(3,2,5);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_onset,:), [-.05 .05])
subplot(3,2,6);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    xForFilter = x'; x_smooth = smooth(xForFilter(:),smoothIndex); x_smooth = reshape(x_smooth', size(xForFilter))';
    imagesc(x_smooth(sortInd_offset,:), [-.2 .2])
colormap('gray')

%% sort according to instantaneous phase (use R2017 smoothing)

smoothIndex = 1;
%clim = [-5*10^-4, 5*10^-4];
clim = [-.2 .2];

sortAtSample_onset = 300;
alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
%alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);
alphaFiltered_onset = smoothdata(alphaFiltered_onset,2,'movmean', 1);
y = hilbert(alphaFiltered_onset); sigphase = angle(y); [~, sortInd_onset] = sort(sigphase(:,sortAtSample_onset), 'ascend');

sortAtSample_offset = 200;
alphaFiltered_offset = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); 
%alphaFiltered_offset = zscore(alphaFiltered_offset, [], 2);
alphaFiltered_offset = smoothdata(alphaFiltered_offset,2,'movmean', 1);
y = hilbert(alphaFiltered_offset); sigphase = angle(y); [~, sortInd_offset] = sort(sigphase(:,sortAtSample_offset), 'ascend');

figure; 
subplot(3,2,1);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_onset,:), clim)
    title('Transition to alpha onset')
subplot(3,2,2);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:)); x = zscore(x, [], 2);
    %x_smooth = smoothdata(x,2,'movmean', smoothIndex);
    x_smooth = smoothdata(x,1,'movmean', 1);
    imagesc(x_smooth(sortInd_offset,:), clim)
    title('Transition to alpha offset')
subplot(3,2,3);
    x = squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_onset,:), clim)
subplot(3,2,4);
    x = squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_offset,:), clim)
subplot(3,2,5);
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,2,'movmean', 50);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_onset,:), clim)
subplot(3,2,6);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:))-squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,2,'movmean', 50);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(x_smooth(sortInd_offset,:), clim)
colormap('gray')

%% align signals to local minimum around indicated time

alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
alphaFiltered_onset = zscore(alphaFiltered_onset, [], 2);

localMimimaOnset = islocalmin(alphaFiltered_onset(:,250:275),2);
localMimimaOnset = localMimimaOnset & cumsum(localMimimaOnset,2) == 1; % get first element in each row
[A, B] = find(localMimimaOnset);
[~, sortInd] = sort(A, 'ascend'); 
Onset_minimum = 250+B(sortInd)-1;

PeriEpisodeERPMerged_trial_onset_shifted = PeriEpisodeERPMerged_trial_onset(:,Onset_minimum-200:Onset_minimum+200);

% figure; cla; hold on; plot(PeriEpisodeERPMerged_trial_onset(9,:)); plot(PeriEpisodeERPMerged_trial_onset(8,:)); 
% figure; cla; hold on; plot(PeriEpisodeERPMerged_trial_onset_notch(9,:)); plot(PeriEpisodeERPMerged_trial_onset_notch(8,:)); 
% 

h = figure('units','normalized','position',[.1 .1 .3 .6]);
subplot(4,2,[1,3]);
    rng(202005)
    time = -250/500:1/500:250/500;
    cla; hold on; 
    patches.timeVec = [-.25 0 .25];
    patches.colorVec = [.8 .8 .8; 1 .8 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [0 500];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    for indTrial = 1:30
        curTrial = randperm(size(PeriEpisodeERPMerged_trial_offset,1));
        curTrial = curTrial(1);
        plot(time, zscore(PeriEpisodeERPMerged_trial_onset(curTrial,:))+indTrial*4, 'k', 'LineWidth', 1);
        %plot(zscore(PeriEpisodeERPMerged_trial_onset_notch(indTrial,:))+indTrial*3, 'r');
    end
    ylim([0 130]); xlabel('Time from indicated alpha onset [s]')
    title('Exemplary alpha onset traces')
    set(gca, 'YTick', []);
subplot(4,2,[2,4]); cla; hold on; 
    rng(202005)
    patches.timeVec = [-.25 0 .25];
    patches.colorVec = [1 .8 .8; .8 .8 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [0 500];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    for indTrial = 1:30
        curTrial = randperm(size(PeriEpisodeERPMerged_trial_offset,1));
        curTrial = curTrial(1);
        plot(time, zscore(PeriEpisodeERPMerged_trial_offset(curTrial,:))+indTrial*4, 'k', 'LineWidth', 1);
        %plot(zscore(PeriEpisodeERPMerged_trial_onset_notch(curTrial,:))+indTrial*4, 'r');
    end
    ylim([0 130]); xlabel('Time from indicated alpha offset [s]')
    title('Exemplary alpha offset traces')
    set(gca, 'YTick', []);

subplot(4,2,5);
    clim = [-1.5 1.5];
    x = squeeze(PeriEpisodeERPMerged_trial_onset(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(time,[],x_smooth(sortInd_onset,:), clim)
    ylabel('Trials')
    set(gca, 'XTick', []);
    xlabel('Time from indicated alpha onset [s]')
    title({'All trials: alpha onset'})
subplot(4,2,6);
    x = squeeze(PeriEpisodeERPMerged_trial_offset(:,:)); x = zscore(x, [], 2);
    %x_smooth = smoothdata(x,2,'movmean', smoothIndex);
    x_smooth = smoothdata(x,1,'movmean', 1);
    imagesc(time,[],x_smooth(sortInd_offset,:), clim)
    ylabel('Trials')
    set(gca, 'XTick', []);
    xlabel('Time from indicated alpha offset [s]')
    title({'All trials: alpha offset'})
    set(gca, 'YTick', []);
subplot(4,2,7);
    x = squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(time,[],x_smooth(sortInd_onset,:), clim)
    ylabel('Trials')
    xlabel('Time from indicated alpha onset [s]')
    title({'All trials:';'alpha onset [8-15 Hz bandstop]'})
subplot(4,2,8);
    x = squeeze(PeriEpisodeERPMerged_trial_offset_notch(:,:)); x = zscore(x, [], 2);
    x_smooth = smoothdata(x,1,'movmean', smoothIndex);
    %x_smooth = smoothdata(x_smooth,1,'movmean', smoothIndex);
    imagesc(time,[],x_smooth(sortInd_offset,:), clim)
    ylabel('Trials')
    xlabel('Time from indicated alpha offset [s]')
    title({'All trials:';'alpha offset [8-15 Hz bandstop]'})
    cb = colorbar('location', 'EastOutside');
    set(get(cb, 'label'), 'string', 'amplitude [z-score]')
    set(gca, 'YTick', []);
    
    set(findall(h,'-property','FontSize'),'FontSize',18)

    addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap']) % add colorbrewer

    cBrew = brewermap(500,'Blues');
    cBrew = flipud(cBrew);
    
    %colormap(gray)
    colormap(cBrew)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'B2_rhythmAlignmentHigh';
    h.InvertHardcopy = 'off';
    h.Color = 'white';

    
    %% align to negative peak (defined based on phase local minimum; cf. Canolty)
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 
    % use only the phase minima durign 250 ms following alpha onset (can try the opposite later)
    sigphase = sigphase(:,251:end-125);
    %sigphase = sigphase(:,126:251);
    
    % get all local minima < -pi+.01
    
    phaseCrit = sigphase<-pi+.01;
    localminCrit = islocalmin(sigphase,2);
    totalCrit = phaseCrit+localminCrit ==2;
    %figure; imagesc(totalCrit(1:30,:))
    
    [xInd, yInd] = find(totalCrit==1);
    
    NotchSignalLocked = NaN(numel(xInd),250);OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        NotchSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset_notch(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
        
    figure; imagesc(zscore(OrigSignalLocked,[],2), [-2, 2])
    figure; imagesc(zscore(NotchSignalLocked,[],2), [-2, 2])
    
    figure; hold on; 
    plot(squeeze(nanmean(zscore(OrigSignalLocked,[],2),1)), 'LineWidth', 2); 
    plot(squeeze(nanmean(zscore(NotchSignalLocked,[],2),1)), 'LineWidth', 2); 
    
    %% get gamma power estimates
    
%     centerFreq = 30:5:150;
%     bandFreq = [centerFreq-10;centerFreq+10];
%     fs = 500;
%     FilterResults = [];
%     for indFreq = 1:numel(centerFreq)
%         % 1. Bandpass for different center frequencies
%         tmp = bandpass(PeriEpisodeERPMerged_trial_onset', bandFreq(:,indFreq), fs);
%         % 2. Normalize; Hilbert transform, magnitude, power
%         tmp = zscore(abs(hilbert(tmp)).^2,[],1);
%         FilterResults(indFreq,:,:) = tmp'; clear tmp;
%     end
%     
    
    centerFreq = 40:4:150;
    bandFreq = [centerFreq-5;centerFreq+5];
    fs = 500;
    FilterResults = [];
    for indFreq = 1:numel(centerFreq)
        % 1. Bandpass for different center frequencies
        tmp = bandpass(PeriEpisodeERPMerged_trial_onset', bandFreq(:,indFreq), fs);
        % 2. Normalize; Hilbert transform, magnitude, power
        tmp = zscore(tmp,[],1);
        tmp = abs(hilbert(tmp)).^2;
        FilterResults(indFreq,:,:) = tmp'; clear tmp;
    end
    
    FreqSignalLocked = NaN(size(FilterResults,1),numel(xInd),250);
    for indSignal = 1:numel(xInd)
        FreqSignalLocked(:,indSignal,:) = FilterResults(:, xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
    
    figure; 
    subplot(3,1,[1,2]); imagesc(squeeze(nanmean(FreqSignalLocked,2))); %xlim([50, 200])
    subplot(3,1,[3]); plot(squeeze(nanmean(zscore(OrigSignalLocked,[],2),1)), 'LineWidth', 2); %xlim([50, 200])
    
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap');
    cBrew = brewermap(1000,'RdBu');
    cBrew = double(flipud(cBrew));
    colormap(cBrew)

    
    time = -125/500:1/500:125/500; time = time(2:end).*1000;
    figure; hold on; 
    plot(time,squeeze(nanmean(OrigSignalLocked,1)),'k', 'LineWidth', 2); ylabel('Mean signal amplitude (normalized)');
    yyaxis right
    plot(time,squeeze(nanmean(nanmean(FreqSignalLocked(10:25,:,:),2),1)), 'LineWidth', 2); 
    ylabel('Gamma amplitude (70-150 Hz; normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlabel('ms from alpha trough')

    
    figure; 
    imagesc(squeeze(nanmean(FilterResults,1)),[0 2])
   
    %% for pre-alpha
    
    alphaFiltered_onset = squeeze(PeriEpisodeERPMerged_trial_onset(:,:))-squeeze(PeriEpisodeERPMerged_trial_onset_notch(:,:)); 
    y = hilbert(alphaFiltered_onset'); sigphase = angle(y)'; 
    % use only the phase minima durign 250 ms following alpha onset (can try the opposite later)
    sigphase = sigphase(:,126:251);
    
    % get all local minima < -pi+.01
    
    phaseCrit = sigphase<-pi+.01;
    localminCrit = islocalmin(sigphase,2);
    totalCrit = phaseCrit+localminCrit ==2;
    %figure; imagesc(totalCrit(1:30,:))
    
    [xInd, yInd] = find(totalCrit==1);
    
    NotchSignalLocked = NaN(numel(xInd),250);OrigSignalLocked = NaN(numel(xInd),250);
    for indSignal = 1:numel(xInd)
        NotchSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset_notch(xInd(indSignal), 125+yInd(indSignal)-125:125+yInd(indSignal)+124);
        OrigSignalLocked(indSignal,:) = PeriEpisodeERPMerged_trial_onset(xInd(indSignal), 125+yInd(indSignal)-125:125+yInd(indSignal)+124);
    end
        
    figure; imagesc(zscore(OrigSignalLocked,[],2), [-2, 2])
    figure; imagesc(zscore(NotchSignalLocked,[],2), [-2, 2])
    
    figure; hold on; plot(squeeze(nanmean(zscore(OrigSignalLocked,[],2),1))); plot(squeeze(nanmean(zscore(NotchSignalLocked,[],2),1)))
    
    
    FreqSignalLocked = NaN(size(FilterResults,1),numel(xInd),250);
    for indSignal = 1:numel(xInd)
        FreqSignalLocked(:,indSignal,:) = FilterResults(:, xInd(indSignal), 250+yInd(indSignal)-125:250+yInd(indSignal)+124);
    end
    
    figure; 
    subplot(3,1,[1,2]); imagesc(squeeze(nanmean(FreqSignalLocked,2))); %xlim([50, 200])
    subplot(3,1,[3]); plot(squeeze(nanmean(zscore(OrigSignalLocked,[],2),1)), 'LineWidth', 2); %xlim([50, 200])
    
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap');
    cBrew = brewermap(1000,'RdBu');
    cBrew = double(flipud(cBrew));
    colormap(cBrew)

    
    time = -125/500:1/500:125/500; time = time(2:end).*1000;
    figure; hold on; 
    plot(time,squeeze(nanmean(OrigSignalLocked,1)),'k', 'LineWidth', 2); ylabel('Mean signal amplitude (normalized)');
    yyaxis right
    plot(time,squeeze(nanmean(nanmean(FreqSignalLocked(20:50,:,:),2),1)), 'LineWidth', 2); 
    ylabel('Gamma amplitude (70-150 Hz; normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlabel('ms from alpha trough')