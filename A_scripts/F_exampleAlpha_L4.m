% Collect individual results

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
pn.dataOut      = [pn.root, 'S4E_PeriRhythmMSE/B_data/A_dataIn/'];

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

PeriEpisodeERPMerged_trial_onset = [];
PeriEpisodeERPMerged_trial_offset = [];
PeriEpisodeERPMerged_trial_onset_notch = [];
PeriEpisodeERPMerged_trial_offset_notch = [];
for indID = 24
    %:numel(IDs)
    for indCond = 4
        load([pn.dataOut, IDs{indID}, '_PeriEpisode_Freq_8_15_L',num2str(indCond),'.mat'], 'PeriEpisodeERP')
       PeriEpisodeERPMerged_trial_onset = cat(1, PeriEpisodeERPMerged_trial_onset, cat(1,PeriEpisodeERP.trial_onset{58:60,1}));
       PeriEpisodeERPMerged_trial_offset = cat(1, PeriEpisodeERPMerged_trial_offset, cat(1,PeriEpisodeERP.trial_offset{58:60,1}));
       PeriEpisodeERPMerged_trial_onset_notch = cat(1, PeriEpisodeERPMerged_trial_onset_notch, cat(1,PeriEpisodeERP.trial_onset_notch{58:60,1}));
       PeriEpisodeERPMerged_trial_offset_notch = cat(1, PeriEpisodeERPMerged_trial_offset_notch, cat(1,PeriEpisodeERP.trial_offset_notch{58:60,1}));
    end
end

% sub 7
figure; 
plot(PeriEpisodeERPMerged_trial_onset(21,:))
plot(PeriEpisodeERPMerged_trial_onset(77,:))


% sub 14

time = -250/500:1/500:250/500;

plot(time, PeriEpisodeERPMerged_trial_onset(73,:), 'k','LineWidth', 2)
plot(time, PeriEpisodeERPMerged_trial_onset(81,:), 'k','LineWidth', 2)


% sub 15

time = -250/500:1/500:250/500;

curSignal1 = PeriEpisodeERPMerged_trial_onset(5,:);
curSignal1(numel(curSignal1)/2+1:end) = NaN;
curSignal2 = PeriEpisodeERPMerged_trial_onset(5,:);
curSignal2(1:numel(curSignal2)/2+1) = NaN;
h = figure('units','normalized','position',[.1 .1 .2 .3]);
    set(0, 'DefaultFigureRenderer', 'painters'); hold on;
    plot(time, curSignal1, 'k','LineWidth', 2)
    plot(time, curSignal2, 'r','LineWidth', 2)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4E_PeriRhythmMSE/C_figures/';
figureName = 'Z_alphaExample';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
    
plot(time, PeriEpisodeERPMerged_trial_onset(5,:), 'k','LineWidth', 2)

plot(time, PeriEpisodeERPMerged_trial_onset(10,:), 'k','LineWidth', 2)
plot(time, PeriEpisodeERPMerged_trial_onset(22,:), 'k','LineWidth', 2)
plot(time, PeriEpisodeERPMerged_trial_onset(119,:), 'k','LineWidth', 2)


figure; 
for indTrial=1:size(PeriEpisodeERPMerged_trial_onset,1)
    plot(PeriEpisodeERPMerged_trial_onset(indTrial,:))
    title(num2str(indTrial));
    pause(1);
end

figure; imagesc(PeriEpisodeERPMerged_trial_onset)
figure; imagesc(PeriEpisodeERPMerged_trial_offset)

figure; plot(squeeze(nanmean(PeriEpisodeERPMerged_trial_onset,1)))

% calculate an FFT of the power spectrum?
